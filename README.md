﻿# Flooding & Spoofing

## VMs selber bauen
### Client VM
```bash
git clone https://gitlab.com/rswrz/lf10-flooding-and-flooding
cd lf10-flooding-and-flooding/client
vagrant up
```
### Server VM
```bash
git clone https://gitlab.com/rswrz/lf10-flooding-and-flooding
cd lf10-flooding-and-flooding/server
curl -o ./app/htdocs/welcome/kali.iso \
  http://cdimage.kali.org/kali-2018.4/kali-linux-mate-2018.4-amd64.iso
vagrant up
```


## Allgemein Infos

- LAN IP Bereich: `172.17.0.0/16`

- System Benutzer auf Server/Client VMs

   - Username: `root`
   - Passwort: `ByByIFI62!`
   - SSH via Putty möglich

- Jede VM hat zwei Interfaces

   - Interface #1: NAT; `eth0`; lediglich zur Verwaltung und Installation
   - Interface #2: Birdge; `eth1`; LAN für das Lab
   - Beide Interfaces **müssen** aktiv bleiben, auch wenn #1 im Lab nicht genutzt wird


## Vorbereitung des Lab

1. Separates Switch
   - Keine Rücksicht auf MAC-Adressen nötig
   - VMs und Hacker an dieses Netz
   - Alle anderen Interfaces an den Rechnern deaktivieren
2. Server VM auf einem deidizierten Rechner starten
3. Client VM auf einem zweiten dedizierten Rechner starten
4. Hacker rufen auffordern http://172.17.13.37 aufzurufen
    - Hier wird die Aufgabe erklärt
    - HTML kann angepasst werden
        - `/app/htdocs/willkommen/index.html` auf Server VM


## Server VM

### Statische IPs

- `172.17.13.37`
- `172.17.13.42`

### DHCP Server

- DHCP Range: `172.17.17.0 - 172.17.17.255`


### Webserver

- Willkommens Seite

    - URI: `http://172.17.13.37`
    - WebRootDir: `/app/htdocs/welcome`
    - Enthält vollwertiges `kali.iso` [3.1 GB] zum Download
        - `http://172.17.13.37/kali.iso`
        - Muss vor dem Build lokal in `/server/app/htdocs/welcome/kali.iso` gespeichert werden

- Geschützter Bereich (HTTP-Auth)
    - URI: `http://172.17.13.42`
    - WebRootDir: `/app/htdocs/secure`
    - HTTP-Auth Benutzer: `Lehrer`
    - HTTP-Auth Password: `RSwrzw4sh3hr3`
    - htpasswd file: `/app/htpasswd`
        - Kann währen der Laufzeit geändert/erweitert werden
- Logs
    - `/app/logs/`
    - Logs live verfolgen: `tail -f /app/logs/*.log`
- Service
    - Nginx
    - `rc-service start/stop/restart`


## Client VM

- Ein Dienst, welcher bei Systemstart automatisch startet
    - `rc-service webclient stop`
    - `rc-service webclient start`
- Authentifizierung an `http://172.17.13.42` alle 3 Sekunden via `curl`
    - `/bin/webclient`
    - `/etc/init.d/webclient`
    - `/etc/conf.d/webclient`
- Bezieht IP-Adresse via DHCP von Server VM


## Hacker

### Neue VM in Virtualbox erstellen
- Als Name der VM  `Kali-Linux` wählen, dann stellt VirtualBox automatisch die wichtigsten Parameter für den Betrieb ein
- Keine virtuelle Festplatte erstellen
- ISO in das virtuelle CD Lauf
- Netzwerk Interface
    - Bridge
    - Passende HW-Netzwerkkarte wählen: INTEL

### Kali Linux starten
1. im Boot Menu `Live` wählen
    - Benutzername: `root`
    - Password: `toor`


#### macof

> CMD Tool für MAC-Flooding

1. Terminal starten
    - `Applications >> Usual applications >> System Tools >> MATE Terminal`
2. Mac Flooding starten
    - `macof --help`
    - `macof -i eth0`
        - Evtl. zu intensives Flooding. Erst recht, wenn der Befehl in einem Netzwerk von mehreren ausgeführt.
    - 10 Anfragen pro Sekunde
        ```bash
        while true; do macof -i eth0 -n 10 && sleep 1; done
       ```

#### Wireshark

> Packet Sniffing

1. Wireshark starten
    - `Applications >> 09 - Sniffing & Spoofing >> Wireshark`
2. Interface wählen und `Capture` starten