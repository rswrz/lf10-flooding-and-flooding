#!/bin/sh

FLAGFILE=/var/run/work-was-already-done

case "$IFACE" in
    lo)
        # The loopback interface does not count.
        # only run when some other interface comes up
        exit 0
        ;;
    *)
        ;;
esac

if [ -e $FLAGFILE ]; then
    exit 0
else
    touch $FLAGFILE
fi

IP_BRIDGE=$(ifconfig eth1 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}')
IP_NAT=$(ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}')

if [ -z "$IP_BRIDGE" ]; then
  sed -i "s/\(Bridge IP:    \).*/\1DOWN/g" /etc/issue
else
  sed -i "s/\(Bridge IP:    \).*/\1$IP_BRIDGE/g" /etc/issue
fi

if [ -z "$IP_NAT" ]; then
  sed -i "s/\(NAT IP:    \).*/\1DOWN/g" /etc/issue
else
  sed -i "s/\(NAT IP:    \).*/\1$IP_NAT/g" /etc/issue
fi

rm $FLAGFILE