#!/bin/sh
while true; do
  curl -H "Client-Vm: CLIENT_VM {LOOP EVERY $3 SECONDS}" -s -u $1 $2 > /dev/null 2>&1
  sleep $3
done