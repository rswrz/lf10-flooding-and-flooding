#!/bin//bash

ROOT_PASSWD='ByByIFI62!'

#
# /etc/issue
#
cat > /etc/issue <<-EOF

                         Role:    Client

                       NAT IP:    0.0.0.0
                    Bridge IP:    0.0.0.0

           Configuration file:    /etc/conf.d/webclient
                 Service file:    /etc/init.d/webclient

           Service start/stop:    rc-service webclient {start,stop}

                  System User:    root
                     Password:    $ROOT_PASSWD

EOF


#
# Hostname
#
echo "client" > /etc/hostname

#
# Network Configuration
#
cat >> /etc/network/interfaces <<-EOF
auto eth1
iface eth1 inet dhcp
EOF

mv /app/if-up-down.sh /etc/network/if-up.d/custom
cp /etc/network/if-up.d/custom /etc/network/if-down.d/custom
chmod +x /etc/network/if-up.d/custom /etc/network/if-down.d/custom
ifup eth1


#
# install cURL Loop
#
mv /app/webclient.sh /bin/webclient
chmod 755 /bin/webclient
chown root:root /bin/webclient

mv /app/webclient.service /etc/init.d/webclient
chmod 755 /etc/init.d/webclient
chown root:root /etc/init.d/webclient

mv /app/webclient.conf /etc/conf.d/webclient
chmod 644 /etc/conf.d/webclient
chown root:root /etc/conf.d/webclient

rc-update add webclient default
rc-service webclient start

rmdir /app



#
# change passwords
#
echo "root:$ROOT_PASSWD" | chpasswd
echo "vagrant:$ROOT_PASSWD" | chpasswd