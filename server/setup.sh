#!/bin//bash

ROOT_PASSWD='ByByIFI62!'

#
# /etc/issue
#
cat > /etc/issue <<-EOF

                        Roles:    Webserver, DHCP

                       NAT IP:    0.0.0.0

                   Static IPs:    172.17.13.37
                                  172.17.13.42

                   DHCP Range:    172.17.17.0 - 172.17.17.255

                 Welcome page:    http://172.17.13.37
                  Secure page:    http://172.17.13.42

             Secure page User:    Lehrer
                     Password:    RSwrzw4sh3hr3

             Webserver htdocs:    /app/htdocs/{welcome,secure}
           Webserver logifles:    /app/logs/{access,error}.log

                  System User:    root
                     Password:    $ROOT_PASSWD

EOF


#
# Hostname
#
echo "server" > /etc/hostname

#
# Network Configuration
#
cat >> /etc/network/interfaces <<-EOF
auto eth1
iface eth1 inet static
    address 172.17.13.37
    netmask 255.255.0.0
iface eth1 inet static
    address 172.17.13.42
    netmask 255.255.0.0
EOF

mv /syscfg/if-up-down.sh /etc/network/if-up.d/custom
cp /etc/network/if-up.d/custom /etc/network/if-down.d/custom
chmod +x /etc/network/if-up.d/custom /etc/network/if-down.d/custom
ifup eth1


#
# DHCP
#
apk add dhcp
mv /syscfg/dhcpd.conf /etc/dhcp/dhcpd.conf
rc-service dhcpd start && rc-update add dhcpd default

#
# NGINX
#
apk add nginx
adduser -D -g 'www' www
chown -R www:www /var/lib/nginx
mkdir -p /app/logs
chown -R www:www /app
mv /syscfg/nginx.conf /etc/nginx/nginx.conf
rc-service nginx start && rc-update add nginx default



#
# change passwords
#
echo "root:$ROOT_PASSWD" | chpasswd
echo "vagrant:$ROOT_PASSWD" | chpasswd



#
# cleanup
#
rmdir /syscfg